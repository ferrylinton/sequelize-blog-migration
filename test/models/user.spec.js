require('module-alias/register');

const assert = require('assert');
const { initDbAsync } = require('@utils/umzug');
const { User } = require('@models/index');

describe('User', function () {

    before(async function () {
        await initDbAsync();
    });

    describe('#findAll()', async function () {

        it(`should return an array of Users`, async function () {
            let users = await User.findAll();
            assert.ok(users.length);
        });

        it(`should return User without Role`, async function () {
            let user = await User.findOne({
                where: {
                    username: 'ferrylinton'
                }
            });
            
            assert.ok(user.role === undefined);
        });

        it(`should return User with Role`, async function () {
            let user = await User.scope('withRole').findOne({
                where: {
                    username: 'ferrylinton'
                }
            });

            assert.ok(user.role);
        });

        it(`should return User with Authorities`, async function () {
            let user = await User.scope('withAuthorities').findOne({
                where: {
                    username: 'ferrylinton'
                }
            });

            assert.ok(user.role.authorities);
        });

    });
});