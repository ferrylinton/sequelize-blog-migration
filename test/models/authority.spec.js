require('module-alias/register');

const assert = require('assert');
const { initDbAsync } = require('@utils/umzug');
const { Authority } = require('@models/index');

describe('Authority', function () {

    before(async function () {
        await initDbAsync();
    });

    describe('#findAll()', async function () {

        it(`should return an array of Authorities`, async function () {
            let authorities = await Authority.findAll();
            assert.ok(authorities.length);
        });

    });
});