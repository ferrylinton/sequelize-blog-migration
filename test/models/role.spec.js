require('module-alias/register');

const assert = require('assert');
const { initDbAsync } = require('@utils/umzug');
const { Role } = require('@models/index');

describe('Role', function () {

    before(async function () {
        await initDbAsync();
    });

    describe('#findAll()', async function () {

        it(`should return an array of Roles`, async function () {
            let roles = await Role.findAll();
            assert.ok(roles.length);
        });

        it(`should return Role without Authorities`, async function () {
            let role = await Role.findOne({
                where: {
                    name: 'Admin'
                }
            });
            
            assert.ok(role.authorities === undefined);
        });

        it(`should return Role with Authorities`, async function () {
            let role = await Role.scope('withAuthorities').findOne({
                where: {
                    name: 'Admin'
                }
            });

            assert.ok(role.authorities);
        });


    });
});