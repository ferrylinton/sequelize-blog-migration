require('module-alias/register');

const assert = require('assert');
const { initDbAsync } = require('@utils/umzug');
const { Image } = require('@models/index');

describe('Image', function () {

    before(async function () {
        await initDbAsync();
    });

    describe('#findAll()', async function () {

        it(`should return an array of Images`, async function () {
            let images = await Image.findAll();
            assert.ok(images);
        });

    });
});