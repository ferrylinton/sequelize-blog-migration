require('module-alias/register');

const assert = require('assert');
const csvService = require('@services/csv-service');


describe('IdService', function () {
    describe('#getData()', function () {

        it('data should has authorities', async function () {
            let data = await csvService.getData();
            assert.ok(data.authorities);
            assert.strictEqual(data.authorities.data.length, data.authorities.length);
        });

    });

});