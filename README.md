# Sequelize Blog Migration

## Running Migrations

Until this step, we haven't inserted anything into the database. We have just created required model and migration files for our first model User. Now to actually create that table in database you need to run db:migrate command.

```
sequelize db:migrate
```

This command will execute these steps:

- Will ensure a table called SequelizeMeta in database. This table is used to record which migrations have run on the current database
- Start looking for any migration files which haven't run yet. This is possible by checking SequelizeMeta table. In this case it will run XXXXXXXXXXXXXX-create-user.js migration, which we created in last step.
- Creates a table called Users with all columns as specified in its migration file.

## Undoing Migrations

Now our table has been created and saved in database. With migration you can revert to old state by just running a command.

You can use db:migrate:undo, this command will revert most recent migration.

```
sequelize db:migrate:undo
```

You can revert back to initial state by undoing all migrations with db:migrate:undo:all command. You can also revert back to a specific migration by passing its name in --to option.

```
sequelize db:migrate:undo:all --to XXXXXXXXXXXXXX-create-posts.js
```

## Running Seeds

In last step you have create a seed file. It's still not committed to database. To do that we need to run a simple command.

```
sequelize db:seed:all
```

This will execute that seed file and you will have a demo user inserted into User table.

Note: Seeder execution history is not stored anywhere, unlike migrations, which use the SequelizeMeta table. If you wish to change this behavior, please read the Storage section.

## Undoing Seeds

Seeders can be undone if they are using any storage. There are two commands available for that:

If you wish to undo the most recent seed:

```
sequelize db:seed:undo
```
If you wish to undo a specific seed:

```
sequelize db:seed:undo --seed name-of-seed-as-in-data
```
If you wish to undo all seeds:

```
sequelize db:seed:undo:all
```