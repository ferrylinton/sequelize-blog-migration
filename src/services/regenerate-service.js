require('dotenv').config();
const csvService = require('@/services/csv-service');
const logger = require('@configs/logger');

    (async () => {

        try {
            logger.info('####### start csvService.regenerate()');
            await csvService.regenerate();
        } catch (err) {
            logger.error(err.stack);
        }

    })()