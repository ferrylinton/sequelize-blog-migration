const fs = require('fs');
const path = require('path');
const csv = require('fast-csv');
const { uid } = require('uid');

var data;

function parse(filename) {
    let obj = {
        data: [],
        length: 0
    }

    return new Promise(function (resolve, reject) {
        let rs = fs.createReadStream(path.resolve(process.cwd(), 'files', 'csv', filename + '.csv'));
        rs
            .pipe(csv.parse({ headers: true }))
            .on('error', error => reject(error))
            .on('data', row => {
                obj.data.push(row);
                data[row.name] = row.id;
            })
            .on('end', (rowCount) => {
                obj.length = rowCount;
                data[filename] = obj;
                resolve(obj);
            });
    })
}

function transform(data, filename) {
    data.map(dt => {
        dt.id = uid();
        return dt
    });

    let ws = fs.createWriteStream(path.resolve(process.cwd(), 'files', 'csv', filename + '.csv'));
    csv.write(data, { headers: true }).pipe(ws);
}

async function getData() {
    if (data) {
        return data;
    } else {
        data = {};
        await parse('authorities');
        await parse('roles');
        await parse('tags');
        return data;
    }
}

async function regenerate() {
    let data = await getData();
    await transform(data['authorities'].data, 'authorities');
    await transform(data['roles'].data, 'roles');
    await transform(data['tags'].data, 'tags');
}

module.exports = {
    regenerate,
    getData
}
