require('module-alias/register');

const bcrypt = require('bcryptjs');
const { uid } = require('uid');
const csvService = require('@services/csv-service');
const passwordHash = bcrypt.hashSync('password', 10);
const DEFAULT_USER = process.env.DEFAULT_USER.split(',');

module.exports = {
  up: async (queryInterface) => {

    let users = [];
    let data = await csvService.getData();

    users.push({
      id: uid(),
      username: 'ferrylinton',
      email: 'ferrylinton@gmail.com',
      password_hash: passwordHash,
      activated: true,
      role_id: data['Admin']
    });

    users.push({
      id: DEFAULT_USER[0],
      username: DEFAULT_USER[1],
      email: 'noreply@bologhu.com',
      password_hash: passwordHash + '111',
      activated: false,
      role_id: data['User']
    })

    await queryInterface.bulkInsert('sec_user', users, {});

  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('sec_user', null, {});
  }
};


