require('module-alias/register');

const csvService = require('@services/csv-service');
const { tableName } = require('@columns/role-authority')


module.exports = {
  up: async (queryInterface) => {

    let roleAuthorities = [];
    let data = await csvService.getData();

    for (let i = 0; i < data.authorities.data.length; i++) {
      roleAuthorities.push({
        role_id: data['Admin'],
        authority_id: data.authorities.data[i].id
      });
    }

    await queryInterface.bulkInsert(tableName, roleAuthorities, {});

  },

  down: async (queryInterface) => {

    let data = await csvService.getData();
    await queryInterface.bulkDelete(tableName, { role_id: data['Admin'] }, {});

  }
};


