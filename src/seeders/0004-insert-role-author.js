require('module-alias/register');

const csvService = require('@services/csv-service');
const { tableName } = require('@columns/role-authority')


module.exports = {
  up: async (queryInterface) => {

    let authorities = ['IMAGE_MODIFY', 'TAG_MODIFY', 'POST_MODIFY', 'LOG_POST_VIEW'];
    let roleAuthorities = [];
    let data = await csvService.getData();

    for (let j = 0; j < authorities.length; j++) {
      roleAuthorities.push({
        role_id: data['Author'],
        authority_id: data[authorities[j]]
      });
    }

    await queryInterface.bulkInsert(tableName, roleAuthorities, {});

  },

  down: async (queryInterface) => {

    let data = await csvService.getData();
    await queryInterface.bulkDelete(tableName, { role_id: data['Author'] }, {});

  }
};


