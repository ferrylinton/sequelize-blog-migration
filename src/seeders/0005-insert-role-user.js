require('module-alias/register');

const csvService = require('@services/csv-service');
const { tableName } = require('@columns/role-authority')


module.exports = {
  up: async (queryInterface) => {

    let authorities = ['AUTHORITY_VIEW', 'ROLE_VIEW', 'USER_VIEW'];
    let roleAuthorities = [];
    let data = await csvService.getData();

    for (let j = 0; j < authorities.length; j++) {
      roleAuthorities.push({
        role_id: data['User'],
        authority_id: data[authorities[j]]
      });
    }

    await queryInterface.bulkInsert(tableName, roleAuthorities, {});

  },

  down: async (queryInterface) => {

    let data = await csvService.getData();
    await queryInterface.bulkDelete(tableName, { role_id: data['User'] }, {});

  }
};


