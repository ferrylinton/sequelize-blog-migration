require('module-alias/register');

const csvService = require('@services/csv-service');
const { tableName } = require('@columns/role')


module.exports = {

  up: async (queryInterface) => {

    let data = await csvService.getData();
    await queryInterface.bulkInsert(tableName, data.roles.data, {});

  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete(tableName, null, {});
  }
};


