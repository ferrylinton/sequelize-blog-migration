require('module-alias/register');

const fs = require('fs');
const path = require('path');
const Umzug = require('umzug');
const logger = require('@configs/logger');
const { sequelize } = require('@models/index');
const SQLITE_DB_FILE = path.join(process.cwd(), process.env.TEST_STORAGE);


async function initDbAsync() {
    if (process.env.NODE_ENV === 'test' &&  process.env.TEST_STORAGE === ':memory:') {
        try {
            await syncSchema(sequelize);
            await syncData(sequelize);
        } catch (err) {
            logger.error(err.stack);
            process.exit(-1);
        }
    } else {
        await Promise.resolve();
    }
}

function initDbSync() {
    if (process.env.NODE_ENV === 'test' && process.env.TEST_STORAGE !== ':memory:') {
        (async () => {

            try {
                await syncSchema(sequelize);
                await syncData(sequelize);
            } catch (err) {
                logger.error(err.stack);
                process.exit(-1);
            }

        })()
    }
}

function deleteSqliteFile() {
    try {
        fs.unlinkSync(SQLITE_DB_FILE)
    } catch (err) {
        logger.error(err);
    }
}

async function syncSchema(sequelize) {
    const umzug = new Umzug({
        migrations: {
            path: path.join(process.cwd(), 'src', 'migrations'),
            params: [
                sequelize.getQueryInterface()
            ]
        },
        storage: 'sequelize',
        storageOptions: {
            sequelize: sequelize,
            modelName: 'SequelizeMeta',
            tableName: 'sequelize_meta',
        }
    });

    await umzug.up();
    logger.info('All migrations performed successfully');
}

async function syncData(sequelize) {
    const umzug = new Umzug({
        migrations: {
            path: path.join(process.cwd(), 'src', 'seeders'),
            params: [
                sequelize.getQueryInterface()
            ]
        },
        storage: 'sequelize',
        storageOptions: {
            sequelize: sequelize,
            modelName: 'SequelizeData',
            tableName: 'sequelize_data'
        }
    });

    await umzug.up();
    logger.info('All seeders performed successfully')
}


module.exports = {
    initDbAsync,
    initDbSync,
    deleteSqliteFile
};