require('module-alias/register');

const fs = require('fs');
const path = require('path');
const sqlite3 = require('sqlite3').verbose();
const logger = require('@configs/logger');
const file = path.join(process.cwd(), process.env.TEST_STORAGE);

function create() {
    if (process.env.NODE_ENV === 'test' &&
        process.env.TEST_STORAGE === 'test.sqlite' &&
        !fs.existsSync(file)) {

        let db = new sqlite3.Database(file, (err) => {
            if (err) {
                logger.error(err.message);
            } else {
                logger.info('DB file created...');
            }
        });


        db.close((err) => {
            if (err) {
                logger.error(err.message);
            }

            logger.info('Close the database connection.');
        });
    }


}

module.exports = {
    create
};

