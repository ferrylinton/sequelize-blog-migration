require('module-alias/register');

const { Model } = require('sequelize');
const { uid } = require('uid');
const { modelName, tableName, camelCase } = require('@columns/log-post');


module.exports = (sequelize, DataTypes) => {

  class LogPost extends Model {

    static associate(models) {
      // 
    }

  };

  LogPost.init(camelCase(), {
    sequelize,
    modelName,
    tableName,
    hooks: {

      beforeCreate: function (instance, options) {
        instance.id = uid();
      }

    }
  });
  
  return LogPost;
};