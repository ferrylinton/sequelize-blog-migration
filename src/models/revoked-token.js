require('module-alias/register');

const { Model } = require('sequelize');
const { uid } = require('uid');
const { modelName, tableName, camelCase } = require('@columns/revoked-token');

module.exports = (sequelize, DataTypes) => {

  class RevokedToken extends Model {

    static associate(models) {
      //
    }

  };

  RevokedToken.init(camelCase(), {
    sequelize,
    modelName,
    tableName,
    hooks: {

      beforeCreate: function (instance, options) {
        instance.id = uid();
      }

    }
  });

  return RevokedToken;
};