require('module-alias/register');

const { Model } = require('sequelize');
const { uid } = require('uid');
const { modelName, tableName, camelCase } = require('@columns/log-update');

module.exports = (sequelize, DataTypes) => {

  class LogUpdate extends Model {

    static associate(models) {
      // 
    }

  };

  LogUpdate.init(camelCase(), {
    sequelize,
    modelName,
    tableName,
    hooks: {

      beforeCreate: function (instance, options) {
        instance.id = uid();
      }

    }
  });

  return LogUpdate;
};