require('module-alias/register');

const { Model } = require('sequelize');
const { uid } = require('uid');
const { modelName, tableName, camelCase } = require('@columns/user-confirmation');


module.exports = (sequelize, DataTypes) => {

  class UserCofirmation extends Model {

    static associate(models) {
      // 
    }

  };

  UserCofirmation.init(camelCase(), {
    sequelize,
    modelName,
    tableName,
    hooks: {

      beforeCreate: function (instance, options) {
        instance.id = uid();
      }

    }
  });

  return UserCofirmation;
};