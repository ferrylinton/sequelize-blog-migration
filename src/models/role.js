require('module-alias/register');

const { Model } = require('sequelize');
const { uid } = require('uid');
const { modelName, tableName, camelCase } = require('@columns/role')

module.exports = (sequelize) => {

  class Role extends Model {

    static associate(models) {

      models.Role.belongsToMany(models.Authority, {
        through: 'sec_role_authority',
        foreignKey: 'role_id',
        as: 'authorities',
        timestamps: false
      });

      models.Role.hasMany(models.User, {
        foreignKey: 'role_id'
      });

      models.Role.addScope('withAuthorities', {
        include: [
          {
            model: models.Authority,
            as: 'authorities',
            attributes: ['id', 'name'],
            through: {
              attributes: []
            }
          }
        ]
      });

    }

  };

  Role.init(camelCase(), {
    sequelize,
    modelName,
    tableName,
    hooks: {

      beforeCreate: function (instance, options) {
        let now = new Date();
        instance.id = uid();
        instance.createdAt = now;
        instance.updatedAt = now;

        if (options.user) {
          let user = options.user.id + ',' + options.user.username;
          instance.createdBy = user;
          instance.updatedBy = user;
        }
      },

      beforeUpdate: function beforeUpdate(instance, options) {
        instance.updatedAt = new Date();

        if (options.user) {
          instance.updatedBy = options.user.id + "," + options.user.username;
        }
      }

    }
  });

  return Role;

};
