require('module-alias/register');

const { Model } = require('sequelize');
const { uid } = require('uid');
const { modelName, tableName, camelCase } = require('@columns/authority');

module.exports = (sequelize) => {

  class Authority extends Model {

    static associate(models) {
      models.Authority.belongsToMany(models.Role, {
        through: 'sec_role_authority',
        foreignKey: 'authority_id',
        as: 'roles',
        timestamps: false
      });
    }

  };

  Authority.init(camelCase(), {
    sequelize,
    modelName,
    tableName,
    hooks: {

      beforeCreate: function (instance, options) {
        let now = new Date();
        instance.id = uid();
        instance.createdAt = now;
        instance.updatedAt = now;

        if (options.user) {
          let user = options.user.id + ',' + options.user.username;
          instance.createdBy = user;
          instance.updatedBy = user;
        }
      },

      beforeUpdate: function beforeUpdate(instance, options) {
        instance.updatedAt = new Date();

        if (options.user) {
          instance.updatedBy = options.user.id + "," + options.user.username;
        }
      }

    }
  });

  return Authority;

};