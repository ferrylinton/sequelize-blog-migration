require('module-alias/register');

const { tableName, snakeCase } = require('@columns/user');
const role = require('@columns/role');

module.exports = {

  up: async (queryInterface) => {

    await queryInterface.createTable(tableName, snakeCase());

    await queryInterface.addConstraint(tableName, {
      type: 'foreign key',
      fields: ['role_id'],
      name: 'sec_user_role_id_fk',
      references: {
        table: role.tableName,
        field: 'id'
      }
    });

  },

  down: async (queryInterface) => {
    await queryInterface.dropTable(tableName);
  }

};