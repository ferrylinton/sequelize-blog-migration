require('module-alias/register');

const { tableName, snakeCase } = require('@columns/log-update');


module.exports = {

  up: async (queryInterface) => {
    await queryInterface.createTable(tableName, snakeCase());
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable(tableName);
  }

};