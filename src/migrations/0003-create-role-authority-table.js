require('module-alias/register');

const { tableName, snakeCase } = require('@columns/role-authority');


module.exports = {
  up: async (queryInterface) => {

    await queryInterface.createTable(tableName, snakeCase());

    await queryInterface.addConstraint(tableName, {
      fields: ['role_id', 'authority_id'],
      type: 'primary key',
      name: 'sec_role_authority_pk'
    });

  },

  down: async (queryInterface) => {
    await queryInterface.dropTable(tableName);
  }

};