require('module-alias/register');

const { tableName, snakeCase } = require('@columns/tag-post');


module.exports = {
  up: async (queryInterface) => {

    await queryInterface.createTable(tableName, snakeCase());

    await queryInterface.addConstraint(tableName, {
      fields: ['tag_id', 'post_id'],
      type: 'primary key',
      name: 'blg_tag_post_pk'
    });

  },

  down: async (queryInterface) => {
    await queryInterface.dropTable(tableName);
  }
  
};