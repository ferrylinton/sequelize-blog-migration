require('module-alias/register');

const _ = require('lodash');
const { DataTypes } = require('sequelize');
const { defaultDatetime } = require('@utils/sequelize');

const modelName = 'RevokedToken';

const tableName = 'sec_revoked_token';

const attributes = {

  id: {
    type: DataTypes.STRING(50),
    primaryKey: true,
  },

  token: {
    type: DataTypes.TEXT,
    allowNull: false
  },

  username: {
    type: DataTypes.STRING(50),
    allowNull: false
  },

  createdAt: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: defaultDatetime()
  }

}

function camelCase() {
  return attributes;
}

function snakeCase() {
  let newAttributes = {};

  for (var key in attributes) {
    newAttributes[_.snakeCase(key)] = attributes[key];
  }

  return newAttributes;
}

module.exports = {
  modelName,
  tableName,
  camelCase,
  snakeCase
};
