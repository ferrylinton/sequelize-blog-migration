require('module-alias/register');

const _ = require('lodash');
const { DataTypes } = require('sequelize');
const authority = require('@columns/authority');
const role = require('@columns/role');
const { create } = require('@columns/commons/audit');

const tableName = 'sec_role_authority';

const attributes = {

  roleId: {
    type: DataTypes.STRING(11),
    allowNull: false,
    references: {
      model: role.tableName,
      key: 'id',
    }
  },

  authorityId: {
    type: DataTypes.STRING(11),
    allowNull: false,
    references: {
      model: authority.tableName,
      key: 'id',
    }
  }

}

function camelCase() {
  return _.merge({}, attributes, create);
}

function snakeCase() {
  let tempAttributes = _.merge({}, attributes, create);
  let newAttributes = {};

  for (var key in tempAttributes) {
    newAttributes[_.snakeCase(key)] = tempAttributes[key];
  }

  return newAttributes;
}

module.exports = {
  tableName,
  camelCase,
  snakeCase
};