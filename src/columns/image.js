require('module-alias/register');

const _ = require('lodash');
const { DataTypes } = require('sequelize');
const { audit } = require('@columns/commons/audit')

const modelName = 'Image';

const tableName = 'blg_image';

const attributes = {

  id: {
    type: DataTypes.STRING(11),
    primaryKey: true
  },

  name: {
    type: DataTypes.STRING(50),
    allowNull: false,
    unique: true
  },

  contentType: {
    type: DataTypes.STRING(50),
    allowNull: false
  },

  bytes: {
    type: DataTypes.BLOB('long'),
    allowNull: false
  },

  url: {
    type: DataTypes.VIRTUAL,
    get() {
      return `${process.env.BASE_URL}/api/images/view/${this.name}`;
    }
  }

}

function camelCase() {
  return _.merge({}, attributes, audit);
}

function snakeCase() {
  let tempAttributes = _.merge({}, attributes, audit);
  let newAttributes = {};

  for (var key in tempAttributes) {
    if(tempAttributes[key].type !== DataTypes.VIRTUAL){
      newAttributes[_.snakeCase(key)] = tempAttributes[key];
    }
  }

  return newAttributes;
}

module.exports = {
  modelName,
  tableName,
  camelCase,
  snakeCase
};
