require('module-alias/register');

const _ = require('lodash');
const { DataTypes } = require('sequelize');
const { defaultDatetime } = require('@utils/sequelize');

const modelName = 'UserConfirmation';

const tableName = 'sec_user_confirmation';

const attributes = {

  id: {
    type: DataTypes.STRING(30),
    primaryKey: true
  },

  ip: {
    type: DataTypes.STRING(50),
    allowNull: true
  },

  userId: {
    type: DataTypes.STRING(11),
    allowNull: true,
  },

  requestDate: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: defaultDatetime()
  },

  status: {
    type: DataTypes.STRING(10),
    allowNull: true
  }

}

function camelCase() {
  return _.merge({}, attributes);
}

function snakeCase() {
  let newAttributes = {};

  for (var key in attributes) {
    newAttributes[_.snakeCase(key)] = attributes[key];
  }

  return newAttributes;
}

module.exports = {
  modelName,
  tableName,
  camelCase,
  snakeCase
};